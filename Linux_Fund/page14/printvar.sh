#!/usr/bin/env bash

# variable that has a value and print it:

answer=42

echo $answer
42

# question number 6:
unset answer 

# question number 3:
MyLANG=$LANG

# question number 4 use the command printenv

# question number 5 export all the shell variables:
# printenv
# env
# export
# declare -x
# declare -xp
