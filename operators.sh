#!/usr/bin/env bash


set -x



# Logical Operators
# and &&
# or ||
# not !


echo 0 && echo 0 ; echo $?
echo 1 || echo 0 ; echo $?
! echo 1 echo 0 ; echo $?


# Existance Operators
# exists -e
# exists and is regular file -f
# exists and is directory -d
# exists and is runnable -x
# exists and is readable -r
# exists and is writable -w
# does not has any value -z
# has some value -n
# exists and is symbolic link -L

echo '-e' && [[ -e /etc/passwd ]]; echo $?
echo '-e' && [[ -f /etc/passwd ]]; echo $?
echo '-e' && [[ -r /etc/passwd ]]; echo $?
echo '-e' && [[ -w /etc/passwd ]]; echo $?
echo '-e' && [[ -x /etc/passwd ]]; echo $?
echo '-e' && [[ -L /etc/passwd ]]; echo $?
echo '-e' && [[ -d /etc/passwd ]]; echo $?


[[ -e /etc/passwd/ ]]; echo $?
[[ -f /etc/passwd/ ]]; echo $?
[[ -r /etc/passwd/ ]]; echo $?
[[ -w /etc/passwd/ ]]; echo $?
[[ -d /etc/passwd/ ]]; echo $?
[[ -x /etc/passwd/ ]]; echo $?
[[ -L /etc/passwd/ ]]; echo $?
[[ -e /etc/passwd/ ]]; echo $?

[[ -z $SHELL ]]; echo $?	
[[ -n $SHELL ]]; echo $?	



