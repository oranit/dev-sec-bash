#!/usr/bin/env bash

####################################
#created by: Oranit Farhi
#purpose: working with getopts
#version: 0.0.1
#date: 07.01.22
####################################

while getopts ":ab" options

do
	case $options in
		a) echo "-a was invoked" ;;
		b) echo "-b was invoked" ;;
		*) echo "invalid option was invoked" ;;
	esac

done
