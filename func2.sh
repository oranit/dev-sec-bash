#!/usr/bin/env bash
############################
#
#
#
############################3


function check_root_home(){ 
	ls -ltRa /root
}

if [[ $EUID -eq 0 ]]; then
	check_root_home
else
	echo "Please run script with root permission"
fi
