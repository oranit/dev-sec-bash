#!/usr/bin/env bash

########################################
#created by: Oranit Farhi
#purpose: working with getopts - optarg
#version: 0.0.1
#date: 07/01/22
########################################

function user_from_passwd(){
	name=$1
	grep $name /etc/passwd
}

while getopts ":a:c:f:h" OPTIONS; do
	case $OPTIONS in
		a) echo "-a was invoked and passed value of $OPTARG" ;;	
		c) echo "-c was invoked and passed value of $OPTARG" ;;
		f) user_from_passwd $OPTARG ;;
		h) echo "usage is -a value -f value -c value -h for help" ;;
		*) echo "incorrect option provide" ;;
	esac
done
