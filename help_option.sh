#!/usr/bin/env bash

####################################
#created by: Oranit Farhi
#purpose: working with getopts
#version: 0.0.1
#date: 07.01.22
####################################

function help(){
	echo -e "Incorrect use:\n $0 -a -b -h"
}


while getopts ":abh" options

do
	case $options in
		a) echo "-a was invoked" ;;
		b) echo "-b was invoked" ;;
		help|h) help ;;
		*) echo "invalid option was invoked"; help ;;
	esac

done
