#!/usr/bin/env bash
set -e
set -u
##############################
#created by Oranit Farhi
#purpose: to work with arrayas
#version; 0.0.1
####################################
name=$1
lname=$2
age=$3

multi=($name $lname $age)

echo "Hello : my name is ${multi[0]} ${multi[1]} and i am ${multi[2]}"
