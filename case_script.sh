#!/usr/bin/env bash
##################################
#created by Oranit Farhi
#purpose: working with case
#version number: 0.0.1
#date: 07/01/2022
##################################

set -o nounset

function upgrade_func(){
	sudo apt-get update
	sudo apt-get upgrade -y
}


while true
do
	read -p "Would you like to upgrade your OS: (YES with capital letters)" ANSWER
	case $ANSWER in
		Y|y|yes|Yes|YES) upgrade_func; break ;;
		N|n|no|No|NO) echo "OK - I get the answer"; break ;;
		*) echo "no problem see you later alligator:-P" ; continue ;;
	esac
done
