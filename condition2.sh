#!/usr/bin/env bash
#set -e
set -e
set -u
set -x
#################################
#created by: oranit farhi



num1=5
num3=4


[ test $num1 -gt $num2 ]; echo $? # integer greate than
[ test $num1 -lt $num2 ]; echo $? # integer less than
[ test $num1 -ge $num2 ]; echo $? # integer greater or equal
[ test $num1 -le $num2 ]; echo $? # less or equal
[ test $num1 -ne $num2 ]; echo $? # integer not equal
[ test $num1 -eq $num2 ]; echo $? # integer equal
