#!/bin/bash
##################################
#Created by: Oranit Farhi
#Purpose: HomeWork
#Version number: 0.0.1 
#Date: 30/12/21
####################################

ls *.txt > /dev/null
if [ $? -ne 0 ]
then
	echo There are 0 .txt files!
else
count=0

for file in *.txt
do
	let count++
done
	echo There are total of $count .txt files in $PWD
fi
