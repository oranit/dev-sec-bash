#!/bin/bash
##################################
#Created by: Oranit Farhi
#Purpose: HomeWork
#Version number: 0.0.1 
#Date: 30/12/21
####################################

i=8

until [[ $i -lt 4 ]]
do 
	echo $i
	i=$((i-1))
done
